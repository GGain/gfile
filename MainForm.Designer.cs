﻿namespace GFiles
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxOpenTextFile = new System.Windows.Forms.TextBox();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.textBoxFileType = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonTextFile = new System.Windows.Forms.Button();
            this.buttonRun = new System.Windows.Forms.Button();
            this.buttonDestFolder = new System.Windows.Forms.Button();
            this.textBoxDestFolder = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonSourceFolder = new System.Windows.Forms.Button();
            this.textBoxSourceFolder = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxIncludeSubfolder = new System.Windows.Forms.CheckBox();
            this.checkBoxOverWrite = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pattern File";
            // 
            // textBoxOpenTextFile
            // 
            this.textBoxOpenTextFile.Location = new System.Drawing.Point(126, 10);
            this.textBoxOpenTextFile.Name = "textBoxOpenTextFile";
            this.textBoxOpenTextFile.Size = new System.Drawing.Size(374, 20);
            this.textBoxOpenTextFile.TabIndex = 1;
            this.textBoxOpenTextFile.Leave += new System.EventHandler(this.textBoxOpenTextFile_Leave);
            // 
            // textBoxFileType
            // 
            this.textBoxFileType.Location = new System.Drawing.Point(126, 82);
            this.textBoxFileType.Name = "textBoxFileType";
            this.textBoxFileType.Size = new System.Drawing.Size(100, 20);
            this.textBoxFileType.TabIndex = 3;
            this.textBoxFileType.Leave += new System.EventHandler(this.textBoxFileType_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "File Type";
            // 
            // buttonTextFile
            // 
            this.buttonTextFile.Location = new System.Drawing.Point(500, 10);
            this.buttonTextFile.Name = "buttonTextFile";
            this.buttonTextFile.Size = new System.Drawing.Size(29, 20);
            this.buttonTextFile.TabIndex = 4;
            this.buttonTextFile.Text = "...";
            this.buttonTextFile.UseVisualStyleBackColor = true;
            this.buttonTextFile.Click += new System.EventHandler(this.buttonTextFile_Click);
            // 
            // buttonRun
            // 
            this.buttonRun.Location = new System.Drawing.Point(125, 113);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(75, 23);
            this.buttonRun.TabIndex = 5;
            this.buttonRun.Text = "Execute";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // buttonDestFolder
            // 
            this.buttonDestFolder.Location = new System.Drawing.Point(500, 58);
            this.buttonDestFolder.Name = "buttonDestFolder";
            this.buttonDestFolder.Size = new System.Drawing.Size(29, 20);
            this.buttonDestFolder.TabIndex = 8;
            this.buttonDestFolder.Text = "...";
            this.buttonDestFolder.UseVisualStyleBackColor = true;
            this.buttonDestFolder.Click += new System.EventHandler(this.buttonDestFolder_Click);
            // 
            // textBoxDestFolder
            // 
            this.textBoxDestFolder.Location = new System.Drawing.Point(126, 58);
            this.textBoxDestFolder.Name = "textBoxDestFolder";
            this.textBoxDestFolder.Size = new System.Drawing.Size(374, 20);
            this.textBoxDestFolder.TabIndex = 7;
            this.textBoxDestFolder.Leave += new System.EventHandler(this.textBoxDestFolder_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Destination Folder";
            // 
            // buttonSourceFolder
            // 
            this.buttonSourceFolder.Location = new System.Drawing.Point(500, 34);
            this.buttonSourceFolder.Name = "buttonSourceFolder";
            this.buttonSourceFolder.Size = new System.Drawing.Size(29, 20);
            this.buttonSourceFolder.TabIndex = 11;
            this.buttonSourceFolder.Text = "...";
            this.buttonSourceFolder.UseVisualStyleBackColor = true;
            this.buttonSourceFolder.Click += new System.EventHandler(this.buttonSourceFolder_Click);
            // 
            // textBoxSourceFolder
            // 
            this.textBoxSourceFolder.Location = new System.Drawing.Point(126, 34);
            this.textBoxSourceFolder.Name = "textBoxSourceFolder";
            this.textBoxSourceFolder.Size = new System.Drawing.Size(374, 20);
            this.textBoxSourceFolder.TabIndex = 10;
            this.textBoxSourceFolder.Leave += new System.EventHandler(this.textBoxSourceFolder_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Source Folder";
            // 
            // checkBoxIncludeSubfolder
            // 
            this.checkBoxIncludeSubfolder.AutoSize = true;
            this.checkBoxIncludeSubfolder.Location = new System.Drawing.Point(420, 85);
            this.checkBoxIncludeSubfolder.Name = "checkBoxIncludeSubfolder";
            this.checkBoxIncludeSubfolder.Size = new System.Drawing.Size(109, 17);
            this.checkBoxIncludeSubfolder.TabIndex = 12;
            this.checkBoxIncludeSubfolder.Text = "Include Subfolder";
            this.checkBoxIncludeSubfolder.UseVisualStyleBackColor = true;
            // 
            // checkBoxOverWrite
            // 
            this.checkBoxOverWrite.AutoSize = true;
            this.checkBoxOverWrite.Location = new System.Drawing.Point(420, 108);
            this.checkBoxOverWrite.Name = "checkBoxOverWrite";
            this.checkBoxOverWrite.Size = new System.Drawing.Size(103, 17);
            this.checkBoxOverWrite.TabIndex = 12;
            this.checkBoxOverWrite.Text = "Overwrite if exist";
            this.checkBoxOverWrite.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 150);
            this.Controls.Add(this.checkBoxOverWrite);
            this.Controls.Add(this.checkBoxIncludeSubfolder);
            this.Controls.Add(this.buttonSourceFolder);
            this.Controls.Add(this.textBoxSourceFolder);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonDestFolder);
            this.Controls.Add(this.textBoxDestFolder);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonRun);
            this.Controls.Add(this.buttonTextFile);
            this.Controls.Add(this.textBoxFileType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxOpenTextFile);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "GFile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxOpenTextFile;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.TextBox textBoxFileType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonTextFile;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.Button buttonDestFolder;
        private System.Windows.Forms.TextBox textBoxDestFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSourceFolder;
        private System.Windows.Forms.TextBox textBoxSourceFolder;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxIncludeSubfolder;
        private System.Windows.Forms.CheckBox checkBoxOverWrite;
    }
}

