﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GFiles
{
    public partial class MainForm : Form
    {

        //string fileType = "All type *.*|*.*";
        string fileType = "*.jpg";
        string srcFolder = null;
        string desFolder = Directory.GetCurrentDirectory();
        string textFile;
        public MainForm()
        {
            InitializeComponent();
            textBoxFileType.Text = fileType;
        }

        private void buttonTextFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Text files *.txt|*.txt";
            openFile.Title = "Select pattern file";
            openFile.RestoreDirectory = true;
            
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                textFile = openFile.FileName;
                textBoxOpenTextFile.Text = textFile;
            }
        }

        private void buttonSourceFolder_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    srcFolder = fbd.SelectedPath;
                    textBoxSourceFolder.Text = srcFolder;
                }
            }
        }

        private void buttonDestFolder_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    desFolder = fbd.SelectedPath;
                    textBoxDestFolder.Text = desFolder;
                }
            }
        }
        
        private void textBoxSourceFolder_Leave(object sender, EventArgs e)
        {
            TextBox tbox = (TextBox)sender;
            if ( !string.IsNullOrWhiteSpace(tbox.Text))
            {
                if (Directory.Exists(tbox.Text))
                {
                    srcFolder = tbox.Text;
                }
                else
                {
                    srcFolder = "";
                    textBoxSourceFolder.Text = "";
                }
                
            }
        }

        private void textBoxDestFolder_Leave(object sender, EventArgs e)
        {
            TextBox tbox = (TextBox)sender;
            if (!string.IsNullOrWhiteSpace(tbox.Text))
            {
                desFolder = tbox.Text;
                if (!Directory.Exists(desFolder))
                {
                    //textBoxDestFolder.Text = "";
                    Directory.CreateDirectory(desFolder);
                }
            }
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            checkBoxIncludeSubfolder.Enabled = false;
            checkBoxOverWrite.Enabled = false;
            System.IO.SearchOption includeSubfolder = SearchOption.TopDirectoryOnly;
            if (checkBoxIncludeSubfolder.Checked)
            {
                includeSubfolder = SearchOption.AllDirectories;
            }
            
            if (Directory.Exists(srcFolder))
            {
                //If Filetype was not inserted, copy all files
                if (string.IsNullOrWhiteSpace(fileType)) fileType = "*.*";
                //If destination folder was not set, create it.
                if (!Directory.Exists(desFolder)) Directory.CreateDirectory(desFolder);
                string[] moveFile=new string[] { }; 
                if (!string.IsNullOrWhiteSpace(textFile) && File.Exists(textFile))
                {
                    foreach (string moveFilePattern in File.ReadLines(textFile, Encoding.UTF8))
                    {
                        moveFile = Directory.GetFiles(srcFolder, moveFilePattern + fileType, includeSubfolder);

                        foreach (string f in moveFile)
                        {
                            string fileName = Path.GetFileName(f);
                            string parentFolder = (new FileInfo(f).Directory).Name;
                            string newFilePath = Path.Combine(desFolder, fileName);
                            //Copy and overwrite if existed
                            if (!checkBoxOverWrite.Checked)
                            {
                                if (File.Exists(newFilePath))
                                {
                                    fileName = parentFolder + "_" + fileName;
                                    newFilePath = Path.Combine(desFolder, fileName);
                                    while (File.Exists(newFilePath))
                                    {
                                        fileName = "(copy) " + fileName;
                                        newFilePath = Path.Combine(desFolder, fileName);
                                    }
                                }
                            }
                            File.Copy(f, newFilePath, checkBoxOverWrite.Checked);
                        }
                    }
                }
                else
                {
                    moveFile = Directory.GetFiles(srcFolder, fileType, includeSubfolder);

                    foreach (string f in moveFile)
                    {
                        string fileName = Path.GetFileName(f);
                        string parentFolder = (new FileInfo(f).Directory).Name;
                        string newFilePath = Path.Combine(desFolder, fileName);
                        //Copy and overwrite if existed
                        if (!checkBoxOverWrite.Checked)
                        {
                            if (File.Exists(newFilePath))
                            {
                                fileName = parentFolder + "_" + fileName;
                                newFilePath = Path.Combine(desFolder, fileName);
                                while (File.Exists(newFilePath))
                                {
                                    fileName = "(copy) " + fileName;
                                    newFilePath = Path.Combine(desFolder, fileName);
                                }
                            }
                        }
                        File.Copy(f, newFilePath, checkBoxOverWrite.Checked);
                    }
                }

                
                MessageBox.Show("Copy done");
            }
            else
            {
                MessageBox.Show("No source folder");
            }
            checkBoxIncludeSubfolder.Enabled = true;
            checkBoxOverWrite.Enabled = true;
        }

        private void textBoxFileType_Leave(object sender, EventArgs e)
        {
            TextBox tbox = (TextBox)sender;
            if (!string.IsNullOrWhiteSpace(tbox.Text))
            {
                fileType = tbox.Text;
            }
            else
            {
                fileType = "*.*";
                textBoxFileType.Text = fileType;
            }
        }

        private void textBoxOpenTextFile_Leave(object sender, EventArgs e)
        {
            TextBox tbox = (TextBox)sender;
            if (!string.IsNullOrWhiteSpace(tbox.Text) && File.Exists(tbox.Text))
            {
                textFile = tbox.Text;
            }
            else
            {
                textFile = null;
                textBoxOpenTextFile.Text = "";
            }
        }
    }
}
